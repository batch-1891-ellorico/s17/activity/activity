

/*
	1. Create a function which is able to prompt the user to 
	provide their fullname, age, and location.
		-use prompt() and store the returned value into a 
		function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	// NAME
	function enterUserName() {
		let userName = prompt('Enter Full Name:')
		console.log('Hello, ' + userName)
	}
	enterUserName()

	// AGE
	function enterUserAge() {
		let userAge = prompt('Enter Age:')
		console.log('You are ' + userAge)
	}
	enterUserAge()

	// LOCATION
	function enterUserLocation() {
		let userLocation = prompt('Enter Location:')
		console.log('You live in ' + userLocation)
	}
	enterUserLocation()

	// THANK YOU
	alert('Thank you for your input!')

/*
	2. Create a function which is able to print/display your 
	top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function enterFavoriteBands(){
		console.log('1. Five Finger Death Punch')
		console.log('2. Green Day')
		console.log('3. Maroon 5')
		console.log('4. IV of Spades')
		console.log('5. December Avenue')
	}
	enterFavoriteBands()

/*
	3. Create a function which is able to print/display 
	your top 5 favorite movies of all time and 
	show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies 
		and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function enterFavoriteMovies(){
		console.log('1. Interstellar')
		console.log('Rotten Tomatoes Rating: 86%')
		console.log('2. Transformers')
		console.log('Rotten Tomatoes Rating: 85%')
		console.log('3. Grave of the Fireflies')
		console.log('Rotten Tomatoes Rating: 95%')
		console.log('4. Avengers: Endgame')
		console.log('Rotten Tomatoes Rating: 90%')
		console.log('5. Thor: Ragnarok')
		console.log('Rotten Tomatoes Rating: 87%')
	}
	enterFavoriteMovies()

/*
	4. Debugging Practice - Debug the following codes and 
	functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	// Global Function
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");



	let printFriends = function(){
		console.log("You are friends with:");
		console.log(friend1);
		console.log(friend2);
		console.log(friend3);
	}

	printFriends()

// console.log(friend1);
// console.log(friend2);

